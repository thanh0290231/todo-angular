import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataService } from 'src/app/data.service';
import { Member } from 'src/app/data/member';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  @Input() listes: Member[];
  filterString = '';
  apiNameList: Member[];
  subscription: Subscription;
  destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.getUsers().pipe(takeUntil(this.destroy$)).subscribe((respon: any[])=>{
      this.listes = respon|| [];
      console.log(respon)
    })
    this.serveUsers();

    // This is for some other question
    this.dataService.getUsers()
      .subscribe((users: any[]) => {
        this.listes = users.filter(user => user.name)
        console.log('Got the users as: ', this.listes);
      })
  }
  serveUsers() {
    this.subscription = this.dataService.serveUsers()
      .subscribe(users => this.apiNameList = users);
  }
  getFilteredUsers() {
    this.listes = this.apiNameList.filter(user => user.name.toLowerCase().indexOf(this.filterString.toLowerCase()) > -1);
    console.log("filteredUsers", this.listes)
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
